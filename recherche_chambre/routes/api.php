<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ChambreController;
use App\Http\Controllers\PhotoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Routes gérant le CRUD des categories de chambre
Route::get('category', [CategoryController::class, 'index']);
Route::post('category', [CategoryController::class, 'store']);
Route::get('category/{id}', [CategoryController::class, 'show']);
Route::get('category/{id}/edit', [CategoryController::class, 'edit']);
Route::put('category/{id}/update', [CategoryController::class, 'update']);
Route::delete('category/{id}/delete', [CategoryController::class, 'delete']);

//Routes gérant le CRUD les photos de chambre
Route::get('photo', [PhotoController::class, 'index']);
Route::post('photo', [PhotoController::class, 'store']);
Route::get('photo/{id}', [PhotoController::class, 'show']);
Route::get('photo/{id}/edit', [PhotoController::class, 'edit']);
Route::put('photo/{id}/update', [PhotoController::class, 'update']);
Route::delete('photo/{id}/delete', [PhotoController::class, 'delete']);

//Routes gérant le CRUD les chambres de chambre
Route::get('chambre', [ChambreController::class, 'index']);
Route::post('chambre', [ChambreController::class, 'store']);
Route::get('chambre/{id}', [ChambreController::class, 'show']);
Route::get('chambre/{id}/edit', [ChambreController::class, 'edit']);
Route::put('chambre/{id}/update', [ChambreController::class, 'update']);
Route::delete('chambre/{id}/delete', [ChambreController::class, 'delete']);
