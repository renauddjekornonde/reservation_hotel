<?php

use App\Http\Controllers\admin_controller;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//route gerant la page d'accueil utilisateur
Route::resource('/', HomeController::class)->names('accueil');

//route gerant la page about utilisateur
Route::get('/about', [HomeController::class, 'about'])->name('about');

//route gerant la page de contact utilisateur
Route::get('/contact', [HomeController::class, 'contact'])->name('contact');

//route la page admin
Route::resource('admin', admin_controller::class)->names('admin');

Route::resource('categorie', CategoryController::class)->names('category');

