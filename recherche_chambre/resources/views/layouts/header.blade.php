<header role="banner">

    <nav class="navbar navbar-expand-md navbar-dark bg-light">
      <div class="container">
        <a class="navbar-brand" href="{{route('accueil.index')}}">LuxuryHotel</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse navbar-light" id="navbarsExample05">
          <ul class="navbar-nav ml-auto pl-lg-5 pl-0">
            <li class="nav-item">
              <a class="nav-link active" href="{{route('accueil.index')}}">Accueil</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="rooms.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Chambre</a>
              <div class="dropdown-menu" aria-labelledby="dropdown04">
                @foreach ($categories as $category)
                <a class="dropdown-item" href="{{route('category.index', $category->id)}}">{{$category->nom}}</a>
                @endforeach
                {{-- <a class="dropdown-item" href="rooms.html">Presidential Room</a>
                <a class="dropdown-item" href="rooms.html">Luxury Room</a>
                <a class="dropdown-item" href="rooms.html">Deluxe Room</a> --}}
              </div>

            </li>
            {{-- <li class="nav-item">
              <a class="nav-link" href="blog.html">Chambre</a>
            </li> --}}
            <li class="nav-item">
              <a class="nav-link" href="{{route('about')}}">A propos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('contact')}}">Contact</a>
            </li>

             <li class="nav-item cta">
              <a class="nav-link" href="booknow.html"><span>Book Now</span></a>
            </li>
          </ul>

        </div>
      </div>
    </nav>
  </header>
