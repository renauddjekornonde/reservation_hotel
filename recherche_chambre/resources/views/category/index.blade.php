@extends('layouts_admin.app')
@section('content')


<div class="mobile-menu-overlay"></div>

<div class="main-container">
    <div class="pd-ltr-20">
        {{-- <div class="card-box pd-20 height-100-p mb-30">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <img src="{{ asset('vendors/images/banner-img.png') }}" alt="">
                </div>
                <div class="col-md-8">
                    <h4 class="font-20 weight-500 mb-10 text-capitalize">
                        Welcome back <div class="weight-600 font-30 text-blue">Johnny Brown!</div>
                    </h4>
                    <p class="font-18 max-width-600">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde hic non repellendus debitis iure, doloremque assumenda. Autem modi, corrupti, nobis ea iure fugiat, veniam non quaerat mollitia animi error corporis.</p>
                </div>
            </div>
        </div> --}}
        @include('layouts_admin.wiged')

        <div class="card-box mb-30">
            <button class="btn btn-success" style="float:right; margin-top: 10px">Ajouter</button>
            <h2 class="h4 pd-20">Categorie</h2>
            <table class="data-table table nowrap">
                <thead>
                    <tr>
                        {{-- <th class="table-plus datatable-nosort">Product</th> --}}
                        <th>Identifiant</th>
                        <th>Nom</th>
                        <th>Date</th>
                        {{-- <th>Modifer</th>
                        <th>Supprimer</th> --}}
                        <th class="datatable-nosort">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="table-plus">
                            {{-- <img src="{{ asset('vendors/images/product-1.jpg') }}" width="70" height="70" alt=""> --}}
                            01
                        </td>
                        <td>
                            <h5 class="font-16">Shirt</h5>
                            by John Doe
                        </td>
                        <td>10/10/2010k</td>
                        
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> View</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Edit</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                   
                </tbody>
            </table>
        </div>

@endsection
