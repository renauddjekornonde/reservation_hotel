<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chambre extends Model
{
    use HasFactory;
    protected $fillable=[
        'id',
        'titre',
        'description',
        'photo',
        'prix',
        'nombrePersonne',
        'status',
        'id_category',
    ];

    public function chambreCategory(){
        return $this->belongsTo(Category::class,'id_category');
    }
}
