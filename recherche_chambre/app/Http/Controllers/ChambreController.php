<?php

namespace App\Http\Controllers;

use App\Models\Chambre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChambreController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $chambres= Chambre::all();
        // if($chambres->count() > 0){
              return response()->json($chambres,200);
        // }else{
        //       return response()->json([
        //         'status'=>404,
        //         'message'=>"No records found!",
        //     ],404);
        // }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator= Validator::make($request->all(),[
           'titre'=>'required|string',
           'description'=>'required|string',
            'prix'=>'required|string',
            'nombrePersonne'=>'required',
           'status'=>'required|string',
            'id_category'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'status'=>422,
                'errors'=>$validator->messages()
            ], 422);
        }else{
            $chambre= Chambre::create([
                'titre'=>$request->titre,
                'description'=>$request->description,
                'prix'=>$request->prix,
                'nombrePersonne'=>$request->nombrePersonne,
                'status'=>$request->status,
                'id_category'=>$request->id_category,
            ]);

            if($chambre){
                return response()->json([
                    'status'=>200,
                    'message'=>"Chambre created successfully"
                ], 200);
            }else{
                return response()->json([
                    'status'=>500,
                    'message'=>"Something went wrong!"
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $chambre= Chambre::find($id);
        if($chambre){
            return response()->json([
                'status'=>200,
                'chambre'=>$chambre
            ],200);
        }
        else{
            return response()->json([
                'status'=>404,
                'message'=>"No such chambre Found!"
            ], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit( $id)
    {
        $chambre= Chambre::find($id);
        if($chambre){
            return response()->json([
                'status'=>200,
                'chambre'=>$chambre
            ],200);
        }
        else{
            return response()->json([
                'status'=>404,
                'message'=>"No such chambre Found!"
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, int $id)
    {
        $validator= Validator::make($request->all(),[
            'titre'=>'required|string',
           'description'=>'required|string',
            'prix'=>'required|string',
            'nombrePersonne'=>'required',
           'status'=>'required|string',
            'id_category'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'status'=>422,
                'errors'=>$validator->messages()
            ], 422);
        }else{
            $chambre= Chambre::find($id);
            if($chambre){
                $chambre->update([
                'titre'=>$request->titre,
                'description'=>$request->description,
                'prix'=>$request->prix,
                'nombrePersonne'=>$request->nombrePersonne,
                'status'=>$request->status,
                'id_category'=>$request->id_category,
            ]);
                return response()->json([
                    'status'=>200,
                    'message'=>"Chambre updated successfully"
                ], 200);
            }else{
                return response()->json([
                    'status'=>404,
                    'message'=>"No such chambre found!"
                ], 404);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $chambre= Chambre::find($id);
        if($chambre){
            $chambre->delete();
            return response()->json([
                'status'=>200,
                'message'=>"Chambre deleted successfully"
            ], 200);
        }else{
            return response()->json([
                'status'=>404,
                'message'=>"No such chambre found!"
            ], 404);
        }
    }
}
