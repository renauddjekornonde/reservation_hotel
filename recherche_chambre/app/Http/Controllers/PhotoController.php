<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $photos= Photo::all();
        if($photos->count() > 0){
              return response()->json([
                'status'=>200,
                'categories'=>$photos,
            ],200);
        }else{
              return response()->json([
                'status'=>404,
                'message'=>"No records found!",
            ],404);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $images= $request->hasFile('image');

        if($images){
            $newFileImage= $request->file('image');
            $file_path_image= $newFileImage->store('public/images/');

            $validator= Validator::make($request->all(),[
                'image'=>'required|string',
                'id_chambre'=>'required',
            ]);

            if($validator->fails()){
                return response()->json([
                    'status'=>422,
                    'errors'=>$validator->messages()
                ], 422);
            }else{
                $photo= Photo::create([
                    'image'=>$file_path_image,
                    'id_chambre'=>$request->id_chambre,
                ]);

                if($photo){
                    return response()->json([
                        'status'=>200,
                        'message'=>"Image created successfully"
                    ], 200);
                }else{
                    return response()->json([
                        'status'=>500,
                        'message'=>"Something went wrong!"
                    ], 500);
                }
            }


        }

    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $photo= Photo::find($id);
        if($photo){
            return response()->json([
                'status'=>200,
                'category'=>$photo
            ],200);
        }
        else{
            return response()->json([
                'status'=>404,
                'message'=>"No such photo Found!"
            ], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $photo= Photo::find($id);
        if($photo){
            return response()->json([
                'status'=>200,
                'category'=>$photo
            ],200);
        }
        else{
            return response()->json([
                'status'=>404,
                'message'=>"No such photo Found!"
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, int $id)
    {
        $images= $request->hasFile('image');
        if($images){
            $newFileImage= $request->file('image');
            $file_path_image= $newFileImage->store('public/images/');

            $validator= Validator::make($request->all(),[
                'image'=>'required',
            ]);

            if($validator->fails()){
                return response()->json([
                    'status'=>422,
                    'errors'=>$validator->messages()
                ], 422);
            }else{
                $photo= Photo::find($id);
                if($photo){
                    $photo->update([
                    'image'=> $file_path_image,
                    'id_chambre'=>$request->id_chambre
                ]);
                    return response()->json([
                        'status'=>200,
                        'message'=>"Image updated successfully"
                    ], 200);
                }else{
                    return response()->json([
                        'status'=>404,
                        'message'=>"No such image found!"
                    ], 404);
                }
        }
    }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $photo= Photo::find($id);
        if($photo){
            $photo->delete();
            return response()->json([
                'status'=>200,
                'message'=>"Image deleted successfully"
            ], 200);
        }else{
            return response()->json([
                'status'=>404,
                'message'=>"No such image found!"
            ], 404);
        }
    }
}
