<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as FacadesValidator;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categories= Category::all();
        if($categories->count() > 0){
              return response()->json([
                'status'=>200,
                'categories'=>$categories,
            ],200);
        }else{
              return response()->json([
                'status'=>404,
                'message'=>"No records found!",
            ],404);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator= FacadesValidator::make($request->all(),[
            'nom'=>'required|string',
        ]);

        if($validator->fails()){
            return response()->json([
                'status'=>422,
                'errors'=>$validator->messages()
            ], 422);
        }else{
            $categorie= Category::create([
                'nom'=>$request->nom
            ]);

            if($categorie){
                return response()->json([
                    'status'=>200,
                    'message'=>"Category created successfully"
                ], 200);
            }else{
                return response()->json([
                    'status'=>500,
                    'message'=>"Something went wrong!"
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $category= Category::find($id);
        if($category){
            return response()->json([
                'status'=>200,
                'category'=>$category
            ],200);
        }
        else{
            return response()->json([
                'status'=>404,
                'message'=>"No such category Found!"
            ], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $category= Category::find($id);
        if($category){
            return response()->json([
                'status'=>200,
                'category'=>$category
            ],200);
        }
        else{
            return response()->json([
                'status'=>404,
                'message'=>"No such category Found!"
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, int $id)
    {
         $validator= FacadesValidator::make($request->all(),[
            'nom'=>'required|string',
        ]);

        if($validator->fails()){
            return response()->json([
                'status'=>422,
                'errors'=>$validator->messages()
            ], 422);
        }else{
            $category= Category::find($id);
            if($category){
                $category->update([
                'nom'=>$request->nom
            ]);
                return response()->json([
                    'status'=>200,
                    'message'=>"Category updated successfully"
                ], 200);
            }else{
                return response()->json([
                    'status'=>404,
                    'message'=>"No such category found!"
                ], 404);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $category= Category::find($id);
        if($category){
            $category->delete();
            return response()->json([
                'status'=>200,
                'message'=>"Category deleted successfully"
            ], 200);
        }else{
            return response()->json([
                'status'=>404,
                'message'=>"No such category found!"
            ], 404);
        }
    }
}
