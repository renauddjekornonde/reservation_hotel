<?php

use App\Http\Controllers\FactureController;
use App\Http\Controllers\ReservationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Routes gérant le CRUD des factures de chambre
Route::get('facture', [FactureController::class, 'index']);
Route::post('facture', [FactureController::class, 'store']);
Route::get('facture/{id}', [FactureController::class, 'show']);
Route::get('facture/{id}/edit', [FactureController::class, 'edit']);
Route::put('facture/{id}/update', [FactureController::class, 'update']);
Route::delete('facture/{id}/delete', [FactureController::class, 'delete']);


//Routes gérant le CRUD des reservation de chambre
Route::get('reservation', [ReservationController::class, 'index']);
Route::post('reservation', [ReservationController::class, 'store']);
Route::get('reservation/{id}', [ReservationController::class, 'show']);
Route::get('reservation/{id}/edit', [ReservationController::class, 'edit']);
Route::put('reservation/{id}/update', [ReservationController::class, 'update']);
Route::delete('reservation/{id}/delete', [ReservationController::class, 'delete']);
