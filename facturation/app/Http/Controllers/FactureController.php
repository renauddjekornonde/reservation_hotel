<?php

namespace App\Http\Controllers;

use App\Models\Facture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FactureController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $factures= Facture::all();
        if($factures->count() > 0){
              return response()->json([
                'status'=>200,
                'factures'=>$factures,
            ],200);
        }else{
              return response()->json([
                'status'=>404,
                'message'=>"No records found!",
            ],404);
        }

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator= Validator::make($request->all(),[
        'mode_payement'=>'required|string',
        'statut'=>'required|string',
        'montant'=>'required',
        'reservation_id'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'status'=>422,
                'errors'=>$validator->messages()
            ], 422);
        }else{
            $facture= Facture::create([
                'mode_payement'=>$request->mode_payement,
                'statut'=>$request->statut,
                'montant'=>$request->montant,
                'reservation_id'=>$request->reservation_id,
            ]);

            if($facture){
                return response()->json([
                    'status'=>200,
                    'message'=>"facture created successfully"
                ], 200);
            }else{
                return response()->json([
                    'status'=>500,
                    'message'=>"Something went wrong!"
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $factures= Facture::find($id);
        if($factures){
            return response()->json([
                'status'=>200,
                'factures'=>$factures
            ],200);
        }
        else{
            return response()->json([
                'status'=>404,
                'message'=>"No such facture Found!"
            ], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $factures= Facture::find($id);
        if($factures){
            return response()->json([
                'status'=>200,
                'factures'=>$factures
            ],200);
        }
        else{
            return response()->json([
                'status'=>404,
                'message'=>"No such facture  Found!"
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, int $id)
    {

        $validator= Validator::make($request->all(),[
        'mode_payement'=>'required|string',
        'statut'=>'required|string',
        'montant'=>'required',
        'reservation_id'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'status'=>422,
                'errors'=>$validator->messages()
            ], 422);
        }else{
            $factures= Facture::find($id);
            if($factures){
                $factures->update([
                    'mode_payement'=>$request->mode_payement,
                    'statut'=>$request->statut,
                    'montant'=>$request->montant,
                    'reservation_id'=>$request->reservation_id,
            ]);
                return response()->json([
                    'status'=>200,
                    'message'=>"Facture updated successfully"
                ], 200);
            }else{
                return response()->json([
                    'status'=>404,
                    'message'=>"No such facture found!"
                ], 404);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $factures= Facture::find($id);
        if($factures){
            $factures->delete();
            return response()->json([
                'status'=>200,
                'message'=>"facture deleted successfully"
            ], 200);
        }else{
            return response()->json([
                'status'=>404,
                'message'=>"No such facture found!"
            ], 404);
        }
    }
}
