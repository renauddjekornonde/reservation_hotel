<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $reservations= Reservation::all();
        if($reservations->count() > 0){
              return response()->json([
                'status'=>200,
                'reservations'=>$reservations,
            ],200);
        }else{
              return response()->json([
                'status'=>404,
                'message'=>"No records found!",
            ],404);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $validator= Validator::make($request->all(),[
            'chambre'=>'required',
            'nbreReservation'=>'required',
            'nbrejours'=>'required',

        ]);

        if($validator->fails()){
            return response()->json([
                'status'=>422,
                'errors'=>$validator->messages()
            ], 422);
        }else{
            $reservations= Reservation::create([
                'chambre'=>$request->chambre,
                'nbreReservation'=>$request->nbreReservation,
                'nbrejours'=>$request->nbrejours,

            ]);

            if($reservations){
                return response()->json([
                    'status'=>200,
                    'message'=>"Reservation created successfully"
                ], 200);
            }else{
                return response()->json([
                    'status'=>500,
                    'message'=>"Something went wrong!"
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $reservations= Reservation::find($id);
        if($reservations){
            return response()->json([
                'status'=>200,
                'reservations'=>$reservations
            ],200);
        }
        else{
            return response()->json([
                'status'=>404,
                'message'=>"No such reservation Found!"
            ], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $reservations= Reservation::find($id);
        if($reservations){
            return response()->json([
                'status'=>200,
                'reservations'=>$reservations
            ],200);
        }
        else{
            return response()->json([
                'status'=>404,
                'message'=>"No such reservation Found!"
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $validator= Validator::make($request->all(),[
            'chambre'=>'required',
            'nbreReservation'=>'required',
            'nbrejours'=>'required',

        ]);

        if($validator->fails()){
            return response()->json([
                'status'=>422,
                'errors'=>$validator->messages()
            ], 422);
        }else{
            $reservations= Reservation::find($id);
            if($reservations){
                $reservations->update([
                    'chambre'=>$request->chambre,
                    'nbreReservation'=>$request->nbreReservation,
                    'nbrejours'=>$request->nbrejours,

            ]);
                return response()->json([
                    'status'=>200,
                    'message'=>"reservation updated successfully"
                ], 200);
            }else{
                return response()->json([
                    'status'=>404,
                    'message'=>"No such reservation found!"
                ], 404);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $reservations= Reservation::find($id);
        if($reservations){
            $reservations->delete();
            return response()->json([
                'status'=>200,
                'message'=>"reservation deleted successfully"
            ], 200);
        }else{
            return response()->json([
                'status'=>404,
                'message'=>"No such reservation found!"
            ], 404);
        }

    }
}
