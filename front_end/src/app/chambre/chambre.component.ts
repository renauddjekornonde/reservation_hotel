import { Component, OnInit } from '@angular/core';
import { DateService } from '../service/date.service';

@Component({
  selector: 'app-chambre',
  templateUrl: './chambre.component.html',
  styleUrls: ['./chambre.component.css']
})
export class ChambreComponent implements OnInit{
chambres:any;
constructor(private dataService:DateService){}

ngOnInit(): void{
 this.getChambreData();
}

getChambreData(){
  this.dataService.getData().subscribe(res=>{
    this.chambres= res;
  })
}

}
